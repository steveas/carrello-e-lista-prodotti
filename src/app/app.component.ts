import { Component } from '@angular/core';
import {Iuser} from './iuser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'es08';
  openEdit: boolean= false;
  openCart: any;
  editProduct!: Iuser;

  selectProduct( item: Iuser){

    this.openEdit = true;
    let obj = Object.assign({}, item);
    console.log(obj);
    this.editProduct = obj;

  }

  CloseEdit(){

    this.openEdit = false;

  }

  OpenCart(){
    this.openCart=true;
  }


}
