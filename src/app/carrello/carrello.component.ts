import { Component, OnChanges, OnInit, DoCheck } from '@angular/core';
import {Iuser} from '../iuser';
import {ProductlistService} from '../productlist.service';

@Component({
  selector: 'app-carrello',
  templateUrl: './carrello.component.html',
  styleUrls: ['./carrello.component.css']
})
export class CarrelloComponent implements OnInit, DoCheck {

  Cart: Iuser[]=[];
  totale!: number;

  constructor(private ProductService: ProductlistService) { }

  ngOnInit(): void {
    this.Cart=this.ProductService.getCart();
    this.totalCart();
  }

  ngDoCheck(){
    this.totalCart();
  }

  totalCart(){
    this.totale=this.ProductService.totalCart();
    console.log(this.totale);
  }
  

  removeToCart(item: Iuser){

      this.ProductService.removeCart(item);
      this.totalCart();
    }

  


}
