import { Component, OnInit, Output , EventEmitter, Input} from '@angular/core';
//import { EventEmitter } from 'stream';
import {Iuser} from '../iuser';
import {ProductlistService} from '../productlist.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  @Output() ProductSelectEmit= new EventEmitter();
  @Output() ProductSendToCartEmit= new EventEmitter();
  @Input() openCart!:boolean;
  products: Iuser[]=[];

  constructor(private ProductService: ProductlistService) {

   }

  ngOnInit(): void {

    this.products=this.ProductService.getProduct();

  }

  removeProduct( item: Iuser ){

    this.ProductService.removeProduct(item);

  }

  selectProduct( item: Iuser ){

    this.ProductSelectEmit.emit(item);

  }

  sendToCart(item: Iuser){

    this.ProductSendToCartEmit.emit(); 
    this.ProductService.addToCart(item);

  }

  

}
