import { Injectable } from '@angular/core';
import {Iuser} from './iuser';

@Injectable({
  providedIn: 'root'
})
export class ProductlistService {

  private products: Iuser[] = [

    { id: 0, name: "Iphone 6", madeIn: "Cina", email: "apple@icloud.com", price: 100},

    { id: 1, name: "Cuffie", madeIn: "Nord-Europa", email: "store@ecommerce.com", price: 5},

    { id: 2, name: "Sony BRAVIA 70'' ", madeIn: "Giappone", email: "e.store-sony@sony.com", price: 1000},
      

  ];

  private Cart: Iuser[]=[

    

    
  ];

  constructor() { }

  getProduct() {
    return this.products;
  }

  removeProduct(item: Iuser) {
    let index = this.products.indexOf(item);
    this.products.splice(index, 1);
  }

  addProduct(item: Iuser) {
    let obj = Object.assign({}, item);
    obj.id = this.products.length;
    console.log(obj);
    this.products.push(obj);
  }

  updateProduct(item: Iuser) {
    let obj = this.products.find(element => element.id === item.id);
    if(obj){
      let index = this.products.indexOf(obj);
      this.products.splice(index, 1, item);
    }
  }

  //funzioni carrello:

  addToCart(item: Iuser) {
    
    this.Cart.push(item);
    
  }

  getCart() {
    return this.Cart;
  }

  removeCart(item: Iuser) {
    let index = this.Cart.indexOf(item);
    this.Cart.splice(index, 1);
  }

  totale: number=0;
  totlist: number=0;

  totalCart(){

    
    this.totale=0;
    for(let i=0; i<this.Cart.length; i++){

      console.log(this.Cart[i].price);
      this.totale += this.Cart[i].price;
      /*this.totlist= this.totlist + this.totale ;
      console.log("totale:");
      console.log(this.totale );
      console.log("totlis" );
      console.log(this.totlist );*/
      
    }
    
    return this.totale;
  }
}
