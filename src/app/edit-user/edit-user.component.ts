import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ProductlistService } from '../productlist.service';
import { Iuser} from '../iuser';
import { Product } from '../product';
//import { EventEmitter } from 'stream';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  @Input() editProduct!: Iuser;
  @Output() ConfirmEditemit= new EventEmitter();

  constructor(private ProductService: ProductlistService) { }

  ngOnInit(): void {
  }

  EditConfirm(){

    this.ConfirmEditemit.emit();

  }

  saveProduct(){
    if(((this.editProduct.name && this.editProduct.madeIn && this.editProduct.email && this.editProduct.price ) == "" || 0 ) || (this.editProduct.price == null ))
     {
        alert("Gli spazi modificati non possono essere modificati con spazi vuoti o con un prezzo inferiore a 1");
    }
    else{

      console.log(this.editProduct);
      this.ProductService.updateProduct(this.editProduct);
      this.EditConfirm();

    }

  }

}
