export interface Iuser {

    id:number,
    name:string,
    madeIn: string,
    email: string,
    price: number
}
