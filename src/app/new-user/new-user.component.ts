import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductlistService  } from '../productlist.service';
import { Iuser} from '../iuser';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  newProduct: Iuser= new Product();

  constructor(private  ProductService :ProductlistService ) { }

  ngOnInit(): void {
  }

  createProduct(){

    if((this.newProduct.name && this.newProduct.madeIn && this.newProduct.email && this.newProduct.price)  === undefined)
     {
        alert("Tutti gli spazi devono essere compilati")
    }
    else{

      this.ProductService.addProduct(this.newProduct);
      this.newProduct = new Product();}
    }

}
