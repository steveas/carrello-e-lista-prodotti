export class Product {

    id!:number;
    name!:string;
    madeIn!: string;
    email!: string;
    price!: number;
}
